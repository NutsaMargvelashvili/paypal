console.log("The extension is up and running")
let transactions = document.getElementsByClassName("ppvx_tile--card___2-9-11")[0];

const transactionHTML = (name, status, date, img, amount, requestStatus) => {
    return "<div>\n" +
        "  <div class=\"transaction-wrapper\" style='display: flex;\n" +
        "    gap: 14px;\n" +
        "    background-color: rgb(228,241,251);\n" +
        "    padding: 0 11px;\n" +
        "    font-size: 14px;\n" +
        "    border-left: 3px solid rgb(28,112,186);'>\n" +
        "    <div  style='display: flex; align-items: center; justify-content: center;' class=\"img-wrapper\">\n" +
        "    <img style='width: 50px;\n" +
        "    height: 50px;\n" +
        "    background: blue;\n" +
        "    border-radius: 50%;' class=\"avatar\" \n" +
        "         src='"+img+"' alt=\"avatar\"/>\n" +
        "    </div>\n" +
        "    <div class=\"user-info\">\n" +
        "      <p class=\"user-name\" style='margin-bottom: 8px; width: 216px; font-weight: 600; margin-top: 16px;'>"+name+"</p>\n" +
        "      <span class=\"status\" style='text-transform: uppercase;\n" +
        "    border: 1px solid black;\n" +
        "    border-radius: 4px;\n" +
        "    padding: 0 8px;\n" +
        "    background-color: #FFF;'>"+ status +"</span>\n" +
        "      <span class=\"date\">"+date+"</span>\n" +
        "      <p class=\"user-name\" style='margin-top: 8px; margin-bottom: 16px;'> "+ requestStatus +"</p>\n" +
        "    </div>\n" +
        "      <div class=\"transaction-amount-wrapper\" style='margin-top: 16px;\n" +
        "    color: rgb(72,112,10);\n" +
        "    font-weight: 600;\n" +
        "    margin-left: 11px;'>\n" +
        "        <p class=\"transaction-amount\" style='display: ruby-text-container;'>+ "+amount +" $</p>\n" +
        "      </div>\n" +
        "  </div>\n" +
        "</div>"
}

 transactions.innerHTML = transactionHTML("George Brown", "Successful", "Yesterday, 07 Jun", "https://media-exp1.licdn.com/dms/image/C5603AQGLXt0VROIMrQ/profile-displayphoto-shrink_200_200/0/1636629752498?e=1658361600&v=beta&t=Zen5A-K0LSjGGrqNTAUquq1Y4NKdIE2r-U9NBS58ItI", 1400, "Received");
transactions.innerHTML = transactions.innerHTML + transactionHTML("Jennifer Williams", "Successful", "Friday, 03 Jun", "https://yt3.ggpht.com/ytc/AKedOLSIPzsJc2HnGdodRYlUOZYiPZ40ttNGXqcP0fVnkg=s900-c-k-c0x00ffffff-no-rj", 705, "Received");

transactions.innerHTML = transactions.innerHTML + transactionHTML("Robert Johnson", "Successful", "Monday, 06 Jun", "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxALCxAQEAgKEBAJDRYNDQkJDRsICQcKIB0iIiAdHx8kKDQsJCYxJx8fLTItMT1AMDAwIys/QT84NzQtMCsBCgoKDg0OGhAQGi0gIB8tLTUtLS8tKy0tKy0tLS0tLS0tLSstLSstKy0rLS0tKystLS04LTgtKy0tLS03LS0rLf/AABEIAMgAyAMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAABAAIDBQYEB//EAEYQAAEEAAMEBgYECwcFAAAAAAEAAgMRBCExBRJBUQYiYXGB8BMykaGx0UJSc8EUIzM0U1RykpOy8RUWQ0RidIIkY5Si4f/EABkBAAIDAQAAAAAAAAAAAAAAAAABAgMEBf/EACARAQEBAAICAwEBAQAAAAAAAAABAgMRITESQVEEEyL/2gAMAwEAAhEDEQA/AFtn8+xH+4k/mK46XZtf88xH+4k/mK5VJmpBGkqRpMiCICFIgJAQEaSARpMxCKACdSQBFKkaQATrI0J+CVJUgDvn6zv3kRK79I/94oAWaAXTHBWZzPuCcnZW9BAX3fpZBWY6xsqf0rv0sn75TaRpWydK7ezvTv8A00v75R/CZP1ib+IVHSRCOgk/C5f1mf8AiH5pHGS/rU/8V3zUNJUgdpxjpv1vEfxXfNA7Qn/XcT/Fd81DSaUh2nO0p/17FfxXfNFcxCSfQ7rl2p+dz/byfzFcwC6tpfnM327/AOYrnVK0giAkiEAqRASRAQCARCVIgIBAJ1JBGkAAEUJHhgtz2gDi47oXM7asA/zMZrgOsUdnJa6wE+OIu7ufAKvwm2sNITcxZun/ABG7rX+KuoZWPbbXtI4FpsKWeqWu59EyMNGQ8eJTk5BWKjSEk5BMAhScgQgG0gnIOcBqR96QIBNITHT8h4nJc73k6nw0Cj8ol8anfIBxvuzSXKQkl8qfxh2P/OJftn/zFQKfG/l5ftX/AMxUNKCVIBEBIIoBIpIhAIBOCQCRIaLJAA1JyACAixWKjgbvSSBoJoXqSqPHdJhpCwk/pJBTfYqjbWIGIxBqVzmtJDCcg0diOHgaG2aJ5EZKrXJ+NGOL7p8mJfPlNI4gH1Tk3wUTWNBoOFfR7+S5sZiLJAsftdZoXG2U13GwRqq/NXySLSGcMc8ZZZ8w9qnwu1TEQWOofVObSqJxfd0eXeExxITkK+m+wO2zYIfYd1tw6sHELSwSiRgc11hwsHsXkuExhYQd8iuWZC2fQ7bAlLoS/Oy6PeyLm8Vo49+eqy8vHOu41RCCDpQOOfIZkKF05Ogr3lW3UZ5LUxNKJ0wGmfuCgcSdTffmgo3SUwc6YnjXdqoyjSSilJ0QCaU9NKRmgJIpJkbi/wAtJ9q74lR0pcR+Vf8AaO+JUaQJEBKkaQCCISpEIAgKk6VSOETWh1B5t7tOqr5jSTQCzvTi42RDdvfJt/AO5IvpLF/6jJhwByzvicirHDx+kaAGyEng128Qodj4A4qcMrL6S9U2HsGOFo/FtscazJWPk5Jl0uLjumCwnRGecghhAP18nLUbM6CANBeM+IP0lu4IGsGTQp9OAWe8trTOLMY/+5cJFFg7MswqvaHQiKjWXYOK9EIXJiIweCj89T7T+Gb9PEOkHRt+E6wBLPaWKr2ViHQTscB6p0P0gvaNsYRsjCHNBBFZ8l5dtbZow0jgASLtruIC08XL34rHz8Ul8NthZ2zRte02Hi+1pUtKg6HYoSwuacnRuzH+laFbZe45up1ejEk4hAoIKQKcUkAqTCn0m8PJQApJKkkwZNnI79t3xTKQe6nu7XH4pwSIk5BOQQUpYoi49nPgE+GC8zkOXErpyaOAHsCnM/pXX4UcYaKA8eJWb6cx70cJvR5y5mloXYgDQX7gs50weXQxk11JNNAEt2fGyHxS/OVB0KhDZHO42Gg8l6ZhhkKXmfRl1ONH1ac7hS0T+l4bTYcO6Q6ekOTKXK5M3WvDt8WpnPluGE6eSpQCVjcD0lme8B8IDdchQWpw+LEjARxF9oULnr2ul79Oh1DVw+S4cTjI2azMHe4BVO3MUQfXILdM6BKzI2cZzbp3c940TalMy+yts9NVjpmSNIbI00NGmyAvP+k0VOBqg4EXwJVsdlOYQ5mJeHszDqFPQ27B6XCEkZx59x4qeJM1Vyd6nlluijzHjw279K0jvK3RWB2RTcbEaNiT/wCLfrfj05XLPIFBFBSVkgikgCdExPOiaUA2kkUEyKbCdY0fpHI5hQ7hZq01z1C7ybKIVlxFc3XE0Xop2BrMybPIZhqnEAdYGR/9XqJ8O6aLfkVH49H8uzXTk6CveVGSTqb95UoYOSIYOSVlOdIgqzpFFv4V3NhB5q5DAo8VhxJE5v1h70rlKa6vbOdHYd+OYD6TAORpXkL48EGtbA10kvVaz1Gud2nkq/otEY55I3NIORAIrebeq2g2QyQ2AL+tW8VzeTXWnZ4Z3ntk9nYufGzSNfgmwtgaTvBrqLwaq+K1PRjeHpA4GmZixRC7Y9mBgsuca4HJvsR2WevIK1NDkFXrU16nS+Z6nm9qraOBOInveysitQCqbbPRtuJDAZpIjFvBz2sL/Tg/BauUGOXMUHHuAKs2MsaIzqy+EtZlnljdl7FdE4lrpAygAHg7hPipNrYQCN4+uw+1ayUUNAs1t6QD2IltqGpPi862Ds+WfEteyFzmYd4Mj9GsW2896b0Y2fvYUbrmgB+++O6dJZ1Uxiz14+5b+HXytjmf1cUxnOv1GgpDH2oej7Qr+qx9mJJ+4eaW4exHVHZnBNKlEZ7PkmmM9iOh2YAgn7h5IpjtIE9gspoUsYVqimE0fFdTCHtzHeORXKdSnxP3T8e0IFgy4Ws2/u8VzkUrMGwmyRB+o8RqEXJTf6rwipZMOW9o5jUKJRWS9uIipIpyCN+4C12ZDgbWy2fICwHmFjNqOL8O4DXByCcV9NvH71ebCxe/GKORHuXJ5s9aru8G+5L+xopnANPd7VU7L2pC0lpkaH2SWu6pJS2jtJkGROZF1oaWRxMssmIzYAxrrt46rgq8zutGrPTTba25HYaxu+Tq4dVrAu3Z+Jc2Nhccnjne4Vj8Zhopg0enPVz3YhTbXXHintYGxzPIYM99hLg0KWs/gnc9xr55ciVi+lWKLYXm8w00tP6bewrXGrLc603lhemeIqLdvOQ13I453VXNeo0mwiINmxPLQHegBLtGsbXFRg3nzzVXHjTjI4YGNc2PDsaJCcvSkBWtLfwYue7ftzf7OabszPUBJJG1oYgQKJQQCQRtAoBJIt1HekgEpgMlGwZqXzmrIqqE6oocUVFJPh5KNHjp2FdIXAuuB+8O0e8KUQ1EoUUuHDsxkfcVKkSmUvTO7Za+AiQNsUWSDVr2Li6KbTDXGIuAMR6ln12LUSgEEEAg8DmCF510iw7sFjN+MED1gBoGrJ/RwyzuN38vPZqZr0KSKPE4pu88VI3/AJFwUs+yIGGzvuvg8+kJXn+zekJdKwl1cN682tW7w+3oXDNuQbq42Subc6y7XHyS+U+BfHm1uGFA/RG5ZXfiJxHC4ljQKrdPqlV0XSHDsdW40WLB0BVTt3pG17C0btH6utJfHVT1yTr2mi2kDAQHACInIGwxYHamKOLxrW3YLwANLFrkk2o9heN/J5Omjgu3o7gHF4mdvDrDdHNauPEzfLBzcl1PDa4HDshbTG1eZJNucV1rmjK6Glb3KEoIkIFAJBEoEIBIFOTXeeCALBmP2gkjF67f2h8UkGfGE9BopPCtUIEQlSVKKRUnNdRB5e8JBKkB3McCLH9CmyFQRSbp7D7lK5OIWdIpCqbbGBE5YT9Elh4ktIIVjjcZHF68zG9hNuPgqaPapxUzWRQH0Ydck0uR7AAqubUmL20fz41rc6n2xe1tjy4KQ9RxYD1Zm+qWqKParmgiz1tDyK9VxOHD2UWggjMEWCFSHo5hXEl2GZmbyyBK5+eSX27N4bPVYmbbBNUPVFN4ClyS42SU6OJcfG16QOi2FBsYVnPsaoXbHw7D1cO0UddSVL5z8R/x191itj7LfPIOocj1qzDAtuzBiGACq3a77td2BwYaKaxoGlNFABTbUjqE0M+A0sqN35iX+fWa5IyumMquwWLZNoc2mnRu6r43KwjXRnlxrOkiBCcUCmQIIoHz2oBFIpJJA6EfjG/tj4pIwflGftt+KSKcSBOQCcrVCFEJBVe09stw5LWgPeNc6ZGe1Qtk9rMy29RagKCfGxRevOwEfRved7Fk8VtOaXWd1H6DOo1cefj2qu8n4unB+1psT0hYL9HE53+p/Uaq2fbk8o3RIGjlENxx8dVWAeeSe0Vx9udlV3dq2cWZ9GPzs2Tke02tD0VjDoO1k9H9mlQO5+3j1V27D2h+BT9azHJk+s65FU8ubrPho4tfHTfmLLRccsGa78NM2aMOZI1zXaOYd4KOVtHRYo6PtWuLw3c4c+O6o/RaZKyc4ca+9RsZvvGWXsVkpVJhog1t0ubaY/Fk8s1YbudKr6T7RjwcBaXNMkg6kOrieZ7Ep5qOrJO6xGPf6PHTFhrdeSC02WlW+B28wgCUEH9IwbzXd44LOEnMk2ZCSeNjily1+9b82yOXvM030E7JW2yVrh/pNlqkKwEMronBzXva4H1mmitBgekINCZtcPSxjLxCtm/1Rris9L5BCKVsjd5r2uB0cw7wTiFNUA0Qvz2IkoeeaRn4b8rH9o34pI4P8tH9qz4hJFSylCcE0IuIAJJoAWTyCuZ1XtfH/g8dNPXk9XjuN5rJubbs/pZ2esXFdW0MSZpXOOjjlyazgFFVAHyAsmtd1v48fGI9xDd86WpdBp3cU7W9MuHC1BYhrwvxSrzyUjRlp460i2vOSAYR2pjxkcrHCs3MUpS183SDRYbES4c3DiHtPON1A94VkzpTjGinGJ/bJHRPiFXvjBObfuIUbsNwEknt0CVzL7iU3qeqtf73y8cJCe4kWp2dM5RpgoB/yJKojhj+lce8B1ImAj/Gd+6LCj/nn8S/23+rCfpLi5L3ZGs3uMLdxzR3qokJcd5zy97jnvHfc49pU3ohWb3u7zQtPDABQb8gpTMnpC6t91C0cS7PiaqgjXJSbtebTSFJE0Zjh8AmmuV/cpC3t+9ADz2IA4bFyQO3o5HNvUDNjx2hX+zukLXkCVoYTkJG5xk9vJZxzK4fJNLcvd3JzViGsTT0A17fEEILNbG2yYqjkzYOq1+phHyWmyOYN3odQQrpe2bWblLgh+Pj+1Z8QgjgR/1EX2rPiEkqlhIFW9IcR6PD7oOc7t3/AI8VZhZnpPOTO1o0iZfe4qzkvWVPFO9RTE9nzRvLj3cio97Mjsy7kXPtt39wtZXQSaAZffZUjRlf9FE36I+/NSuPDl7CgFYB08NEhQ7OHNIacb7skTp3eBQDT5pGvJGaNUdddUD4IMCa7va600a+bTiOXPuCTT2fIpAG1ppSDh4+5OIz1+8Jc8vvpMGE/wBPWKaB7FIRrmm1Y7LvlaCKstGkfAJjtVIB3D3G0zz3hABrQK58+SBGfcj5yQdYz5IBbvnmEGtHnQo72Wo+8Jgv+vAoCNxq+w+IV50f2pRELzkco3HVp5KjdocuPgm3u0bz1BGRtSl6R1n5Tp6Ps/8AOYftmfzBJQdEMYzEmEud+MjmY17TkCbFHxRVl8qOOXzFkNnzfqeI/hFY7bOzMVJiZSNm4wjeoH0DyHNGXJFJLeu4lx8czVY/ZOKBB/s3Gf8Ajv09iY7ZmJFj+z8ZoD+bv+SCSqXpotnzgl34Bi/qtHoHD7lM3Z81Z4PE8/yTur7kkkAfwOUf5afxidn7kDhZTrBNn/23fJFJAMGHf+gl7eoRSb6F4/wX/uEFJJBgWEV+Lf4tN2hRvNru4gigkkgjSPPAJF1ebzSSQYEg9teFJo11+aSSAN+exA+aSSQQHIDzSBGXz4JJICHs094CDj55hBJMGuPV7Ky+iXKFrrPw5hJJBL3obiTFtPD61LPGxw73CkkklLsvjH//2Q==", 966, "Received");

transactions.innerHTML = transactions.innerHTML + transactionHTML("James Smith", "Successful", "Yesterday, 07 Jun", "https://media-exp1.licdn.com/dms/image/C5603AQFyvZNa8L_ECA/profile-displayphoto-shrink_200_200/0/1628144805612?e=1657152000&v=beta&t=l0tyyS1ok9-EPOXqhWICqM-Jn6xiRh7FHQAOllNhbUc", 1110, "Sent");
transactions.innerHTML = transactions.innerHTML + transactionHTML("Maria Rodriguez", "Successful", "Yesterday, 07 Jun", "https://media-exp1.licdn.com/dms/image/C4D03AQHAuKYYN2gseQ/profile-displayphoto-shrink_200_200/0/1643912536778?e=1658361600&v=beta&t=_8_Fug0ISAkzzYJxcmI-T-lHbdfBPSRwynp5gv4gbXY", 400, "Sent");





